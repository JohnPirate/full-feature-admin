<?php

/**
 * This file is part of the full feature admin package
 *
 * @license MIT
 * @package FFA
 */

return [
    /*
    |---------------------------------------------------------------------------
    | Method to be called in the middleware return case
    | Available: abort|redirect
    |---------------------------------------------------------------------------
    */
    'middleware_handling' => 'redirect',

    /*
    |---------------------------------------------------------------------------
    | Parameter passed to the middleware_handling method
    |---------------------------------------------------------------------------
    */
    'middleware_params' => 'user-locked',
];
