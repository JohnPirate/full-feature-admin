<?php

namespace FFA;

/**
 * This file is part of Laratrust,
 * a role & permission management solution for Laravel.
 *
 * @license MIT
 * @package FFA
 */

use Illuminate\Console\Command;

class SetupCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'ffa:setup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Setup migration and models for full feature admin package';

    /**
     * Commands to call with their description
     *
     * @var array
     */
    protected $calls = [
        'ffa:role' => 'Creating Role model',
        'ffa:permission' => 'Creating Permission model',
        'ffa:add-trait' => 'Adding LaratrustUserTrait to User model'
    ];

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function fire()
    {
        foreach ($this->calls as $command => $info) {
            $this->line(PHP_EOL . $info);
            $this->call($command);
        }
    }
}
