<?php

namespace FFA;

/**
 * This file is part of the full feature admin package
 *
 * @license MIT
 * @package FFA
 */

use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Config;
use FFA\Traits\ExpandedUserTrait;
use Traitor\Traitor;

class AddExpandedTraitUseCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'ffa:add-trait';

    /**
     * Trait added to User model
     *
     * @var string
     */
    protected $targetTrait = ExpandedUserTrait::class;

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function fire()
    {
        $models = $this->getUserModels() ;

        foreach ($models as $model) {
            if (!class_exists($model)) {
                $this->error("Class $model does not exist.");
                return;
            }

            if ($this->alreadyUsesExpandedUserTrait($model)) {
                $this->error("Class $model already uses ExpandedUserTrait.");
                continue;
            }

            Traitor::addTrait($this->targetTrait)->toClass($model);
        }

        $this->info("ExpandedUserTrait added successfully to {$models->implode(', ')}");
    }

    /**
     * @param  string $model
     * @return bool
     */
    protected function alreadyUsesExpandedUserTrait($model)
    {
        return in_array(ExpandedUserTrait::class, class_uses($model));
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return "Add ExpandedUserTrait to {$this->getUserModels()->implode(', ')} class";
    }

    /**
     * @return array
     */
    protected function getUserModels()
    {
        return new Collection(Config::get('ffa.user_models', []));
    }
}
