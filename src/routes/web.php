<?php

/*
|---------------------------------------------------------------------------
| FullFeatureAdmin Routes
|---------------------------------------------------------------------------
|
| Add this routs to your web routs file
|
*/


Route::get('/missing-permission', function () { return view('errors.missing-permission'); });
Route::get('/user-locked', function () { return view('errors.user-locked'); });

Route::group(['prefix' => 'admin'], function () {

    // Permissions
    Route::get('/permissions', 'PermissionController@index');
    Route::get('/permissions/create', 'PermissionController@create');
    Route::post('/permissions', 'PermissionController@store');
    Route::get('/permissions/{id}', 'PermissionController@detail');
    Route::get('/permissions/{id}/edit', 'PermissionController@edit');
    Route::put('/permissions/{id}', 'PermissionController@update');
    Route::delete('/permissions/{id}', 'PermissionController@delete');
    Route::put('/permissions/{id}/roles', 'PermissionController@updateRoles');

    // Roles
    Route::get('/roles', 'RoleController@index');
    Route::get('/roles/create', 'RoleController@create');
    Route::post('/roles', 'RoleController@store');
    Route::get('/roles/{id}', 'RoleController@detail');
    Route::get('/roles/{id}/edit', 'RoleController@edit');
    Route::put('/roles/{id}', 'RoleController@update');
    Route::delete('/roles/{id}', 'RoleController@delete');
    Route::put('/roles/{id}/permissions', 'RoleController@updatePermissions');

    // Users
    Route::get('/users', 'UserController@index');
    Route::get('/users/{id}', 'UserController@showUser');
    Route::get('/users/{id}/edit', 'UserController@editUser');
    Route::put('/users/{id}', 'UserController@updateUser');

    // User account locking
    Route::patch('/users/{id}/lock', 'UserController@lockUser');
    Route::patch('/users/{id}/unlock', 'UserController@unlockUser');
});
