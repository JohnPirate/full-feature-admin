/**
 * TableCheckboxes
 * @version 0.0.1
 */
export default class {
  constructor (table_id = null) {
    this.table_id = table_id;
    this.table = null;
    this.master_checkbox = null;

    this.init();

    // console.info('tableCheckbox init');
  }

  init () {
    var vm = this;
    this.table = document.getElementById(this.table_id);

    // Find master checkbox
    this.master_checkbox = this.table.querySelector('thead input[type=checkbox]');

    // Set master checkbox checked if all other checkboxes are checked
    if (this.allCheckboxesChecked()) {
      this.master_checkbox.checked = true;
    }

    // Add event listener to master checkbox
    this.master_checkbox.addEventListener('click', function () {
      if (this.checked === true) {
        vm.setCheckboxesChecked();
      } else {
        vm.setCheckboxesUnchecked();
      }
    });

    // Add event listener to other checkboxes
    let checkboxes = this.getAllCheckboxes();
    for (let iter = 0; iter < checkboxes.length; iter++) {
      checkboxes[iter].removeEventListener('click', this.toggleMasterCheckbox.bind(this));
      checkboxes[iter].addEventListener('click', this.toggleMasterCheckbox.bind(this));
    }
  }

  getMasterCheckbox () {
    return this.master_checkbox;
  }

  getAllCheckboxes () {
    return this.table.querySelectorAll('tbody input[type=checkbox]');
  }

  allCheckboxesChecked () {
    let checkboxes = this.getAllCheckboxes();
    for (let iter = 0; iter < checkboxes.length; iter++) {
      if (!checkboxes[iter].checked) {
        return false;
      }
    }
    return true;
  }

  toggleMasterCheckbox () {
    this.master_checkbox.checked = !!this.allCheckboxesChecked();
  }

  toggleCheckboxes (value = false) {
    let checkboxes = this.getAllCheckboxes();
    for (let iter = 0; iter < checkboxes.length; iter++) {
      checkboxes[iter].checked = value;
    }
  }

  setCheckboxesChecked() {
    this.toggleCheckboxes(true);
  }

  setCheckboxesUnchecked() {
    this.toggleCheckboxes(false);
  }
};