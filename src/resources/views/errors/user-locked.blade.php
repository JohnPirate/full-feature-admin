@extends('layouts.app')

@section('content')
    <div class="container animated fadeIn">
        <div class="row">
            <div class="col-sm-12 text-center">
                <div class="well">
                    <div class="gui-empty-state">
                        <div class="gui-empty-state__icon">
                            <i class="fa fa-lock"></i>
                        </div>
                        <div class="gui-empty-state__title">
                            Sorry, but you are locked...
                        </div>
                        <div class="gui-empty-state__message">
                            If you don't know why you are locked, please contact your administrator!
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection