@extends('layouts.app')

@section('content')
    <div class="container animated fadeIn">
        <div class="row">
            <div class="col-sm-12 text-center">
                <div class="well">
                    <div class="gui-empty-state">
                        <div class="gui-empty-state__icon">
                            <i class="fa fa-exclamation-circle"></i>
                        </div>
                        <div class="gui-empty-state__title">
                            Missing Permission!
                        </div>
                        <div class="gui-empty-state__message">
                            You are not allowed to perform this action! Please contact the administrator!
                        </div>
                        <div class="gui-empty-state__action">
                            <a href="{{ url('home') }}" title="Go back to dashboard">Back to dashboard</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection