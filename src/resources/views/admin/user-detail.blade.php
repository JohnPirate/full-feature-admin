@extends('layouts.app')

@section('content')
    @include('layouts.admin-menu')
    <div class="container animated fadeIn">
        <div class="row">
            <div class="col-sm-12 col-md-4 col-lg-3">
                <ul class="list-inline no-margin head-line-ext">
                    <li><p class="h3">Benutzer Übersicht</p></li>
                </ul>
            </div>

            <div class="col-sm-12 col-md-8 col-lg-9 text-right">
                <ul class="list-inline no-margin head-line-ext">
                    <li>
                        <a href="{{ url(sprintf('admin/users/%s/edit', $user->id)) }}"
                           class="btn btn-border-success @if (!\Laratrust::hasRole(['owner', 'administrator'])) disabled @endif"
                           title="Benutzer bearbeiten"
                        >
                            Bearbeiten
                        </a>
                    </li>
                    <li><a href="{{ url('admin/users') }}" class="btn btn-default" title="Zurück zur Benutzerliste">Zurück</a></li>
                </ul>
            </div>
        </div>
    </div>

    <hr>

    <div class="container animated fadeIn">
        <div class="row">
            <div class="col-sm-12">
                @include('alert::alert')
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">

                <div class="row">
                    <div class="col-sm-12 col-md-4 col-lg-3">
                        <h4><i class="fa fa-user-circle-o gui-margin-medium-right"></i>Allgemeines</h4>
                        <p class="text-muted">Allgemeine Daten zum Benutzer</p>
                    </div>

                    <div class="col-sm-12 col-md-8 col-lg-9 gui-form-section--border-left">

                        <table class="gui-table table-hover">
                            <tbody>
                            <tr>
                                <td class="col-sm-6 col-lg-4"><strong>ID</strong></td>
                                <td>{{ $user->id }}</td>
                            </tr>
                            <tr>
                                <td><strong>Name</strong></td>
                                <td>{{ $user->name }}</td>
                            </tr>
                            <tr>
                                <td><strong>E-Mail</strong></td>
                                <td>{{ $user->email }}</td>
                            </tr>
                            <tr>
                                <td><strong>Erstellt</strong></td>
                                <td>{{ $user->created_at }}</td>
                            </tr>
                            <tr>
                                <td><strong>Geändert</strong></td>
                                <td>{{ $user->updated_at }}</td>
                            </tr>
                            <tr>
                                <td><strong>E-Mail bestätigt</strong></td>
                                <td>
                                    @if ($user->email_confirmed )
                                        <i class="fa fa-check"></i>
                                    @else
                                        <i class="fa fa-times"></i>
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td><strong>Telefonnummer bestätigt</strong></td>
                                <td>
                                    @if ($user->phone_number_confirmed )
                                        <i class="fa fa-check"></i>
                                    @else
                                        <i class="fa fa-times"></i>
                                    @endif
                                </td>
                            </tr>
                            </tbody>
                        </table>

                    </div>
                </div>

                <hr>

                <div class="row">
                    <div class="col-sm-12 col-md-4 col-lg-3">
                        <h4><i class="fa fa-shield gui-margin-medium-right"></i>Sicherheit</h4>
                        <p class="text-muted">Sicherheitsinformationen zum Account</p>
                    </div>

                    <div class="col-sm-12 col-md-8 col-lg-9 gui-form-section--border-left">

                        <table class="gui-table table-hover">
                            <tbody>
                            <tr>
                                <td class="col-sm-6 col-lg-4"><strong>Zweifaktor Authentifizierung</strong></td>
                                <td>
                                    @if ($user->two_factor_enabled )
                                        <i class="fa fa-check"></i>
                                    @else
                                        <i class="fa fa-times"></i>
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td><strong>Account gesperrt</strong></td>
                                <td>
                                    @if ($user->lockout_enabled )
                                        <i class="fa fa-check"></i>
                                    @else
                                        <i class="fa fa-times"></i>
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td><strong>Dauer der Sperre</strong></td>
                                <td>{{ $user->lockout_end }}</td>
                            </tr>
                            <tr>
                                <td><strong>Anzahl fehlgeschlagener Anmeldungen</strong></td>
                                <td>{{ $user->access_failed_count }}</td>
                            </tr>
                            <tr>
                                <td><strong>Benutzerrolle</strong></td>
                                <td>
                                    @if(count($user->roles) > 0)
                                        {{ $user->roles[0]->display_name }}
                                    @endif
                                </td>
                            </tr>
                            </tbody>
                        </table>

                    </div>
                </div>

            </div>

        </div>
    </div>
@endsection

@section('page-script')

@endsection