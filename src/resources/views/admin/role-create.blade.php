@extends('layouts.app')

@section('content')
    @include('layouts.admin-menu')

    <div class="container animated fadeIn">
        <div class="row">
            <div class="col-sm-12 col-md-4">
                <ul class="list-inline no-margin head-line-ext">
                    <li>
                        <p class="h3">Neue Benutzerrolle</p>
                    </li>
                </ul>
            </div>
            <div class="col-sm-12 col-md-8 text-right">
                <ul class="list-inline no-margin head-line-ext">

                </ul>
            </div>
        </div>
    </div>

    <hr>

    <div class="container animated fadeIn">
        <div class="row">
            <div class="col-sm-12">
                @include('alert::alert')
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">

                <div class="row">
                    <div class="col-sm-12 col-md-4 col-lg-3">
                        <h4><i class="fa fa-user-plus gui-margin-medium-right"></i>Allgemeines</h4>
                        <p class="text-muted">Allgemeine Daten zur Benutzerrolle</p>
                    </div>
                    <div class="col-sm-12 col-md-8 col-lg-9 gui-form-section--border-left">
                        <form role="form" method="POST" action="{{ url('/admin/roles') }}">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('roleName') ? ' has-error' : '' }}">
                                <label for="roleName">Name <span class="text-warning">*</span></label>
                                <input id="roleName" type="text" class="form-control" name="roleName" value="{{ old('roleName') }}" autofocus>
                                @if ($errors->has('roleName'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('roleName') }}</strong>
                                    </span>
                                @endif
                            </div>


                            <div class="form-group{{ $errors->has('roleDisplayName') ? ' has-error' : '' }}">
                                <label for="roleDisplayName">Display Name <span class="text-warning">*</span></label>
                                <input id="roleDisplayName" type="text" class="form-control" name="roleDisplayName" value="{{ old('roleDisplayName') }}">
                                @if ($errors->has('roleDisplayName'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('roleDisplayName') }}</strong>
                                    </span>
                                @endif
                            </div>


                            <div class="form-group{{ $errors->has('roleDescription') ? ' has-error' : '' }}">
                                <label for="roleDescription">Beschreibung <span class="text-warning">*</span></label>
                                <textarea id="roleDescription" type="text" class="form-control" name="roleDescription" rows="5">{{ old('roleDescription') }}</textarea>
                                @if ($errors->has('roleDescription'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('roleDescription') }}</strong>
                                    </span>
                                @endif
                            </div>


                            <div class="form-group">
                                <button class="btn btn-border-success" type="submit" title="Neue Benutzerrolle anlegen">Anlegen</button>
                                <a href="{{ url('/admin/roles') }}" title="Zurück zur Übersicht" class="btn btn-default">Abbrechen</a>
                            </div>

                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
