@extends('layouts.app')

@section('content')
    @include('layouts.admin-menu')

    <div class="container animated fadeIn">
        <div class="row">
            <div class="col-sm-12 col-md-4">
                <ul class="list-inline no-margin head-line-ext">
                    <li>
                        <p class="h3">Neue Berechtigung</p>
                    </li>
                </ul>
            </div>
            <div class="col-sm-12 col-md-8 text-right">
                <ul class="list-inline no-margin head-line-ext">

                </ul>
            </div>
        </div>
    </div>

    <hr>

    <div class="container animated fadeIn">
        <div class="row">
            <div class="col-sm-12">
                @include('alert::alert')
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">

                <div class="row">
                    <div class="col-sm-12 col-md-4 col-lg-3">
                        <h4><i class="fa fa-gavel gui-margin-medium-right"></i>Allgemeines</h4>
                        <p class="text-muted">Allgemeine Daten zu den Berechtigungen</p>
                    </div>
                    <div class="col-sm-12 col-md-8 col-lg-9 gui-form-section--border-left">
                        <form role="form" method="POST" action="{{ url('/admin/permissions') }}">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('permissionName') ? ' has-error' : '' }}">
                                <label for="permissionName">Name <span class="text-warning">*</span></label>
                                <input id="permissionName" type="text" class="form-control" name="permissionName" value="{{ old('permissionName') }}" autofocus>
                                @if ($errors->has('permissionName'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('permissionName') }}</strong>
                                    </span>
                                @endif
                            </div>


                            <div class="form-group{{ $errors->has('permissionDisplayName') ? ' has-error' : '' }}">
                                <label for="permissionDisplayName">Display Name <span class="text-warning">*</span></label>
                                <input id="permissionDisplayName" type="text" class="form-control" name="permissionDisplayName" value="{{ old('permissionDisplayName') }}">
                                @if ($errors->has('permissionDisplayName'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('permissionDisplayName') }}</strong>
                                    </span>
                                @endif
                            </div>


                            <div class="form-group{{ $errors->has('permissionDescription') ? ' has-error' : '' }}">
                                <label for="permissionDescription">Beschreibung <span class="text-warning">*</span></label>
                                <textarea id="permissionDescription" type="text" class="form-control" name="permissionDescription" rows="5">{{ old('permissionDescription') }}</textarea>
                                @if ($errors->has('permissionDescription'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('permissionDescription') }}</strong>
                                    </span>
                                @endif
                            </div>


                            <div class="form-group">
                                <button class="btn btn-border-success" type="submit" title="Neu Berechtigung anlegen">Anlegen</button>
                                <a href="{{ url('/admin/permissions') }}" title="Zurück zur Übersicht" class="btn btn-default">Abbrechen</a>
                            </div>

                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
