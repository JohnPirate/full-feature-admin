@extends('layouts.app')

@section('content')
    @include('layouts.admin-menu')

    <div class="modal fade" tabindex="-1" role="dialog" id="deletePermissionModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Rolle löschen</h4>
                </div>
                <div class="modal-body">
                    <p>Möchten Sie wirklich die Berechtigung <strong>{{ $permission->display_name }}</strong> löschen?</p>
                </div>
                <div class="modal-footer">
                    <form method="POST" action="{{ url(sprintf('/admin/permissions/%s', $permission->id)) }}">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                        <button type="button" class="btn btn-default" data-dismiss="modal" title="Löschvorgang abbrechen">Abbrechen</button>
                        <button type="submit" class="btn btn-danger" title="Löschvorgang bestätigen">Löschen</button>
                    </form>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->



    <div class="container animated fadeIn">
        <div class="row">
            <div class="col-sm-12 col-md-4">
                <ul class="list-inline no-margin head-line-ext">
                    <li>
                        <p class="h3">Berechtigung bearbeiten</p>
                    </li>
                </ul>
            </div>
            <div class="col-sm-12 col-md-8 text-right">
                <ul class="list-inline no-margin head-line-ext">

                </ul>
            </div>
        </div>
    </div>

    <hr>

    <div class="container animated fadeIn">
        <div class="row">
            <div class="col-sm-12">
                @include('alert::alert')
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">

                <div class="row">
                    <div class="col-sm-12 col-md-4 col-lg-3">
                        <h4><i class="fa fa-gavel gui-margin-medium-right"></i>Allgemeines</h4>
                        <p class="text-muted">Allgemeine Daten zu den Berechtigungen</p>
                    </div>
                    <div class="col-sm-12 col-md-8 col-lg-9 gui-form-section--border-left">
                        <form role="form" method="POST" action="{{ url(sprintf('/admin/permissions/%s', $permission->id)) }}">
                            {{ method_field('PUT') }}
                            {{ csrf_field() }}

                            <div class="form-group">
                                <label for="permissionName">Name</label>
                                <p id="permissionName" class="form-control-static text-mono"><code>{{ $permission->name }}</code></p>
                                <span class="help-block"><small>Der Name kann nicht mehr geändert werden</small></span>
                            </div>


                            <div class="form-group{{ $errors->has('permissionDisplayName') ? ' has-error' : '' }}">
                                <label for="permissionDisplayName">Display Name <span class="text-warning">*</span></label>
                                <input id="permissionDisplayName" type="text" class="form-control" name="permissionDisplayName" value="{{ $permission->display_name }}">
                                @if ($errors->has('permissionDisplayName'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('permissionDisplayName') }}</strong>
                                    </span>
                                @endif
                            </div>


                            <div class="form-group{{ $errors->has('permissionDescription') ? ' has-error' : '' }}">
                                <label for="permissionDescription">Beschreibung <span class="text-warning">*</span></label>
                                <textarea id="permissionDescription" type="text" class="form-control" name="permissionDescription" rows="5">{{ $permission->description }}</textarea>
                                @if ($errors->has('permissionDescription'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('permissionDescription') }}</strong>
                                    </span>
                                @endif
                            </div>


                            <div class="form-group">
                                <button class="btn btn-border-success" type="submit" title="Berechtigung speichern">Speichern</button>
                                <button class="btn btn-border-danger btn-permission-delete" type="button" title="Berechtigung löschen">Löschen</button>
                                <a href="{{ url('/admin/permissions') }}" title="Zurück zur Übersicht" class="btn btn-default">Abbrechen</a>
                            </div>

                        </form>
                    </div>
                </div>

            </div>

            <div class="col-sm-12">
                <hr>
            </div>


            <div class="col-sm-12">

                <div class="row">
                    <div class="col-sm-12 col-md-4 col-lg-3">
                        <h4><i class="fa fa-user-plus gui-margin-medium-right"></i>Berechtigungen</h4>
                        <p class="text-muted">
                            Hier können der Berechtigungen Benutzerrolle zugeteilt werden
                        </p>
                    </div>
                    <div class="col-sm-12 col-md-8 col-lg-9 gui-form-section--border-left">
                        @if ($roles !== null)
                            <form method="POST" action="{{ url(sprintf('/admin/permissions/%s/roles', $permission->id)) }}">
                                {{ method_field('PUT') }}
                                {{ csrf_field() }}

                                <table class="gui-table table-hover" id="update-permission-roles">
                                    <thead>
                                    <tr>
                                        <th class="table--min-width">
                                            <input type="checkbox">
                                        </th>
                                        <th class="text-uppercase">Benutzerrollen</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($roles as $role)
                                        <tr>
                                            <td>
                                                <input type="checkbox"
                                                       name="roles[{{ $role->id }}]"
                                                       @if (in_array($role->id, $permission_role_ids))
                                                            checked
                                                       @endif
                                                >
                                            </td>
                                            <td>{{ $role->display_name }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>

                                <div class="form-group">
                                    <button type="submit" class="btn btn-border-success" title="Benutzerrollen aktualisieren">Aktualisieren</button>
                                </div>
                            </form>
                        @else
                            @include('components.roles-empty-state')
                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-script')
    <script>
      $(document).ready(function () {
        $('.btn-permission-delete').on('click', function () {
          $('#deletePermissionModal').modal('show');
        });

        var table_id = 'update-permission-roles';
        tableCheckbox(table_id);
      });
    </script>
@endsection