@extends('layouts.app')

@section('content')
    @include('layouts.admin-menu')

<div class="modal fade" tabindex="-1" role="dialog" id="deletePermissionModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Berechtigung löschen</h4>
            </div>
            <div class="modal-body">
                <p>Möchten Sie wirklich die Berechtigung löschen?</p>
            </div>
            <div class="modal-footer">
                <form method="POST" data-action="{{ url('/admin/permissions') }}">
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}
                    <button type="button" class="btn btn-default" data-dismiss="modal" title="Löschvorgang abbrechen">Abbrechen</button>
                    <button type="submit" class="btn btn-danger" title="Löschvorgang bestätigen">Löschen</button>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div class="container animated fadeIn">
    <div class="row">
        <div class="col-sm-12 col-md-4">
            <ul class="list-inline no-margin head-line-ext">
                <li>
                    <p class="h3">Berechtigungen</p>
                </li>
            </ul>
        </div>
        <div class="col-sm-12 col-md-8 text-right">
            <ul class="list-inline no-margin head-line-ext">
                <li><a href="{{ url('/admin/permissions/create') }}" class="btn btn-border-success" title="Neue Berechtigung anlegen">Neue Berechtigung</a></li>
            </ul>
        </div>
    </div>
</div>

<hr>

<div class="container animated fadeIn">
    <div class="row">
        <div class="col-sm-12">
            @include('alert::alert')
        </div>
    </div>

    <div class="row">

        <div class="col-sm-12">
            {{-- Filter options --}}
        </div>

        <div class="col-sm-12">
            <form method="GET" action="{{ url('/admin/permissions') }}">
                <div class="form-group">
                    <div class="input-group">
                        <input type="text" class="form-control" name="query" value="{{ $query or '' }}">
                        <span class="input-group-btn">
                                <button class="btn btn-default" type="submit"><i class="fa fa-search gui-margin-medium-right"></i>Suche</button>
                            </span>
                    </div>
                </div>
            </form>
            @if(count($permissions) > 0)
                <table class="gui-table table-hover" id="permissions-table">
                    <thead>
                    <tr>
                        <th class="table--min-width">ID</th>
                        <th>Display Name</th>
                        <th>Name</th>
                        <th class="table--min-width nowrap"></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($permissions as $permission)
                    <tr data-target-id="{{ $permission->id }}">
                        <td>{{ $permission->id }}</td>
                        <td>{{ $permission->display_name }}</td>
                        <td class="text-mono"><code>{{ $permission->name}}</code></td>
                        <td class="nowrap">
                            <a href="{{ url(sprintf('/admin/permissions/%s/edit', $permission->id)) }}" class="btn btn-default" title="Berechtigung bearbeiten">
                                <i class="fa fa-pencil-square-o"></i>
                            </a>
                            <button class="btn btn-border-danger btn-delete-permission" title="Berechtigung löschen">
                                <i class="fa fa-trash-o"></i>
                            </button>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
                @if (isset($query))
                    {{ $permissions->appends(['query' => $query])->links() }}
                @else
                    {{ $permissions->links() }}
                @endif
            @elseif(isset($query))
                @include('components.permissions-query-empty-state')
            @else
                @include('components.permissions-empty-state')
            @endif
        </div>
    </div>
</div>
@endsection

@section('page-script')
<script>
    $(document).ready(function () {
        $('#permissions-table').on('click', '.btn-delete-permission', function () {showDeleteModal($(this), '#deletePermissionModal')});

//      $('.selectpicker').selectpicker({
//        style: 'btn-default',
//        size: 5
//      });

    });
</script>
@endsection
