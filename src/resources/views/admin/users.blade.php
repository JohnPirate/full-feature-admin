@extends('layouts.app')

@section('content')
    @include('layouts.admin-menu')

    <div class="container animated fadeIn">
        <div class="row">
            <div class="col-sm-12 col-md-4">
                <ul class="list-inline no-margin head-line-ext">
                    <li>
                        <p class="h3">Benutzer</p>
                    </li>
                </ul>
            </div>
            <div class="col-sm-12 col-md-8 text-right">
                <ul class="list-inline no-margin head-line-ext">

                </ul>
            </div>
        </div>
    </div>

    <hr>

    <div class="container animated fadeIn">
        <div class="row">
            <div class="col-sm-12">
                @include('alert::alert')
            </div>
        </div>

        <div class="row">

            <div class="col-sm-12">
                {{-- Filter options --}}
            </div>

            <div class="col-sm-12">
                <form method="GET" action="{{ url('/admin/users') }}">
                    <div class="form-group">
                        <div class="input-group">
                            <input type="text" class="form-control" name="query" value="{{ $query or '' }}">
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="submit"><i class="fa fa-search gui-margin-medium-right"></i>Suche</button>
                            </span>
                        </div>
                    </div>
                </form>
                @if(count($users) > 0)
                    <table class="gui-table table-hover" id="roles-table">
                        <thead>
                            <tr>
                                <th class="table--min-width">ID</th>
                                <th>User</th>
                                <th>Email</th>
                                <th>Role</th>
                                <th class="table--min-width "></th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $user)
                            <tr>
                                <td>{{ $user->id }}</td>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->email }}</td>
                                <td>
                                    @if(count($user->roles) > 0)
                                        {{ $user->roles[0]->display_name }}
                                    @endif
                                </td>
                                <td class="text-nowrap">
                                    <a href="{{ url(sprintf('admin/users/%s', $user->id)) }}" title="Benutzer Profil" class="btn btn-border-info">
                                        <i class="fa fa-id-card-o"></i>
                                    </a>
                                    <a href="{{ url(sprintf('admin/users/%s/edit', $user->id)) }}" title="Benutzer Bearbeiten" class="btn btn-default">
                                        <i class="fa fa-pencil-square-o"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                    @if (isset($query))
                        {{ $users->appends(['query' => $query])->links() }}
                    @else
                        {{ $users->links() }}
                    @endif
                @elseif(isset($query))
                    <div class="well">
                        <div class="gui-empty-state">
                            <div class="gui-empty-state__icon">
                                <i class="fa fa-id-card"></i>
                            </div>
                            <div class="gui-empty-state__title">
                                Nichts gefunden...
                            </div>
                            <div class="gui-empty-state__message">
                                Es sind keine Benutzer mit Namen <strong>{{ $query }}</strong> vorhanden!
                            </div>
                        </div>
                    </div>
                @else
                    <div class="well">
                        <div class="gui-empty-state">
                            <div class="gui-empty-state__icon">
                                <i class="fa fa-id-card"></i>
                            </div>
                            <div class="gui-empty-state__title">
                                Keine Benutzer...
                            </div>
                            <div class="gui-empty-state__message">
                                Es sind noch keine Benutzer vorhanden!
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
