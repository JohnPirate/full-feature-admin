@extends('layouts.app')

@section('content')
    @include('layouts.admin-menu')

    <div class="container animated fadeIn">
        <div class="row">
            <div class="col-sm-12 col-md-4 col-lg-3">
                <ul class="list-inline no-margin head-line-ext">
                    <li><p class="h3">Benutzer Bearbeiten</p></li>
                </ul>
            </div>

            <div class="col-sm-12 col-md-8 col-lg-9 text-right">
                <ul class="list-inline no-margin head-line-ext">

                </ul>
            </div>
        </div>
    </div>

    <hr>

    <div class="container animated fadeIn">
        <div class="row">
            <div class="col-sm-12">
                @include('alert::alert')
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">

                <div class="row">
                    <div class="col-sm-12 col-md-4 col-lg-3">
                        <h4><i class="fa fa-user-circle-o gui-margin-medium-right"></i>Allgemeines</h4>
                        <p class="text-muted">Allgemeine Daten zum Benutzer</p>
                    </div>
                    <div class="col-sm-12 col-md-8 col-lg-9 gui-form-section--border-left">

                        <div class="form-group">
                            <label for="permissionName">ID</label>
                            <p id="permissionName" class="form-control-static">{{ $user->id }}</p>
                            <span class="help-block"><small>Die ID kann nicht geändert werden</small></span>
                        </div>

                        <div class="form-group">
                            <label for="permissionName">Name</label>
                            <p id="permissionName" class="form-control-static">{{ $user->name }}</p>
                            <span class="help-block"><small>Der Name kann nicht geändert werden</small></span>
                        </div>


                        <div class="form-group">
                            <label for="permissionName">E-Mail</label>
                            <p id="permissionName" class="form-control-static">{{ $user->email }}</p>
                            <span class="help-block"><small>Die E-Mail Adresse kann nicht geändert werden</small></span>
                        </div>


                        <div class="form-group">
                            <a href="{{ url(sprintf('admin/users/%s', $user->id)) }}" title="Zurück zur Übersicht" class="btn btn-default">Abbrechen</a>
                        </div>

                    </div>
                </div>

            </div>


            <div class="col-sm-12">
                <hr>
            </div>


            <div class="col-sm-12">

                <div class="row">
                    <div class="col-sm-12 col-md-4 col-lg-3">
                        <h4><i class="fa fa-shield gui-margin-medium-right"></i>Sicherheit</h4>
                        <p class="text-muted">
                            Hier können Sicherheitseinstellungen zum Benutzer verändert werden.
                        </p>
                    </div>
                    <div class="col-sm-12 col-md-8 col-lg-9 gui-form-section--border-left">

                        <div class="form-group{{ $errors->has('userLockTime') ? ' has-error' : '' }}">
                            <label>Benutzersperre <span class="text-warning">*</span></label>
                            <div>
                                <form method="POST" action="{{ url(sprintf('admin/users/%s/lock', $user->id)) }}" class="inline-block">
                                    {{ method_field('PATCH') }}
                                    {{ csrf_field() }}
                                    <select name="userLockTime"
                                            class="selectpicker user-lock-time"
                                            title="Dauer der Benutzersperre"
                                            @if($user->isLocked() || $user->hasRole('owner')) disabled @endif
                                    >
                                        <option value="0" selected>dauerhaft</option>
                                        <option value="1">1 Minute</option>
                                        <option value="5">5 Minuten</option>
                                        <option value="10">10 Minuten</option>
                                        <option value="60">1 Stunde</option>
                                        <option value="1440">1 Tag</option>
                                        <option value="10080">1 Woche</option>
                                    </select>
                                    <button type="submit"
                                            class="btn btn-border-danger"
                                            title="Benutzer sperren"
                                            @if($user->isLocked() || $user->hasRole('owner')) disabled @endif
                                    >
                                        <i class="fa fa-lock gui-margin-medium-right"></i>Sperren
                                    </button>
                                </form>

                                <form method="POST" action="{{ url(sprintf('admin/users/%s/unlock', $user->id)) }}" class="inline-block">
                                    {{ method_field('PATCH') }}
                                    {{ csrf_field() }}
                                    <button type="submit"
                                            class="btn btn-border-success"
                                            title="Benutzer entsperren"
                                            @if(!$user->isLocked()) disabled @endif
                                    >
                                        <i class="fa fa-unlock gui-margin-medium-right"></i>Entsperren
                                    </button>
                                </form>
                            </div>
                            @if ($errors->has('userLockTime'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('userLockTime') }}</strong>
                                    </span>
                            @endif
                        </div>

                    </div>
                </div>
            </div>


            <div class="col-sm-12">
                <hr>
            </div>


            <div class="col-sm-12">

                <div class="row">
                    <div class="col-sm-12 col-md-4 col-lg-3">
                        <h4><i class="fa fa-cog gui-margin-medium-right"></i>Erweiterte Einstellungen</h4>
                        <p class="text-muted">
                            Hier können administrative Einstellungen zum Benutzer verändert werden.
                        </p>
                    </div>
                    <div class="col-sm-12 col-md-8 col-lg-9 gui-form-section--border-left">
                        <form method="POST" action="{{ url(sprintf('admin/users/%s', $user->id)) }}">
                            {{ method_field('PUT') }}
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('userRole') ? ' has-error' : '' }}">
                                <label>Benutzerrolle <span class="text-warning">*</span></label>
                                <select name="userRole"
                                        class="selectpicker user-role"
                                        title="Benutzergruppe wählen"
                                        @if($user->hasRole('owner')) disabled @endif
                                >
                                    @foreach($roles as $role)
                                        <option value="{{ $role->name }}"
                                            @if (count($user->roles) > 0 && $role->id === $user->roles[0]->id)
                                                selected
                                            @endif
                                        >
                                            {{ $role->display_name }}
                                        </option>
                                    @endforeach
                                </select>
                                @if ($errors->has('userRole'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('userRole') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <button type="submit"
                                        class="btn btn-border-success"
                                        title="Benutzerrolle aktualisieren"
                                        @if($user->hasRole('owner')) disabled @endif
                                >
                                    Aktualisieren
                                </button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection

@section('page-script')
    <script>
      $('.selectpicker.user-lock-time').selectpicker({
        style: 'btn-default',
        size: 5,
        width: '200px'
      });

      $('.selectpicker.user-role').selectpicker({
        style: 'btn-default',
        size: 5,
        width: '100%'
      });
    </script>
@endsection