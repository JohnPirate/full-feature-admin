@extends('layouts.app')

@section('content')
    @include('layouts.admin-menu')

<div class="modal fade" tabindex="-1" role="dialog" id="deleteRoleModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Rolle löschen</h4>
            </div>
            <div class="modal-body">
                <p>Möchten Sie wirklich die Benutzerrolle löschen?</p>
            </div>
            <div class="modal-footer">
                <form method="POST" data-action="{{ url('/admin/roles') }}">
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}
                    <button type="button" class="btn btn-default" data-dismiss="modal" title="Löschvorgang abbrechen">Abbrechen</button>
                    <button type="submit" class="btn btn-danger" title="Löschvorgang bestätigen">Löschen</button>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div class="container animated fadeIn">
    <div class="row">
        <div class="col-sm-12 col-md-4">
            <ul class="list-inline no-margin head-line-ext">
                <li>
                    <p class="h3">Benutzerrollen</p>
                </li>
            </ul>
        </div>
        <div class="col-sm-12 col-md-8 text-right">
            <ul class="list-inline no-margin head-line-ext">
                <li><a href="{{ url('/admin/roles/create') }}" class="btn btn-border-success" title="Neue Benutzerrolle anlegen">Neue Benutzerrolle</a></li>
            </ul>
        </div>
    </div>
</div>

<hr>

<div class="container animated fadeIn">
    <div class="row">
        <div class="col-sm-12">
            @include('alert::alert')
        </div>
    </div>

    <div class="row">

        <div class="col-sm-12">
            {{-- Filter options --}}
        </div>

        <div class="col-sm-12">
            @if(count($roles) > 0)
                <table class="gui-table table-hover" id="roles-table">
                    <thead>
                    <tr>
                        <th class="table--min-width">ID</th>
                        <th>Display Name</th>
                        <th>Name</th>
                        <th>Description</th>
                        <th class="table--min-width nowrap"></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($roles as $role)
                    <tr data-target-id="{{ $role->id }}">
                        <td>{{ $role->id }}</td>
                        <td>{{ $role->display_name }}</td>
                        <td class="text-mono"><code>{{ $role->name}}</code></td>
                        <td>{{ $role->description}}</td>
                        <td class="nowrap">
                            <a href="{{ url(sprintf('/admin/roles/%s/edit', $role->id)) }}" class="btn btn-default" title="Benutzerrolle bearbeiten">
                                <i class="fa fa-pencil-square-o"></i>
                            </a>
                            <button class="btn btn-border-danger btn-role-permission" title="Benutzerrolle löschen">
                                <i class="fa fa-trash-o"></i>
                            </button>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            @else
                @include('components.roles-empty-state')
            @endif
        </div>
    </div>
</div>
@endsection

@section('page-script')
<script>
    $(document).ready(function () {
        $('#roles-table').on('click', '.btn-role-permission', function () {showDeleteModal($(this), '#deleteRoleModal')});
    });
</script>
@endsection
