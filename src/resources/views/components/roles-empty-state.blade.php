<div class="well">
    <div class="gui-empty-state">
        <div class="gui-empty-state__icon">
            <i class="fa fa-user-plus"></i>
        </div>
        <div class="gui-empty-state__title">
            Keine Benutzerrollen...
        </div>
        <div class="gui-empty-state__message">
            Es sind noch keine Benutzerrollen vorhanden!
        </div>
        <div class="gui-empty-state__action">
            <a href="{{ url('/admin/roles/create') }}" class="btn btn-border-success" title="Neue Benutzerrolle erstellen">Benutzerrolle erstellen</a>
        </div>
    </div>
</div>