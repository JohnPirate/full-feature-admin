<div class="well">
    <div class="gui-empty-state">
        <div class="gui-empty-state__icon">
            <i class="fa fa-gavel"></i>
        </div>
        <div class="gui-empty-state__title">
            Nichts gefunden...
        </div>
        <div class="gui-empty-state__message">
            Es sind keine Berechtigungen mit Namen <strong>{{ $query }}</strong> vorhanden!
        </div>
    </div>
</div>