<div class="well">
    <div class="gui-empty-state">
        <div class="gui-empty-state__icon">
            <i class="fa fa-gavel"></i>
        </div>
        <div class="gui-empty-state__title">
            Keine Berechtigungen...
        </div>
        <div class="gui-empty-state__message">
            Es sind noch keine Berechtigungen vorhanden!
        </div>
        <div class="gui-empty-state__action">
            <a href="{{ url('/admin/permissions/create') }}" class="btn btn-border-success" title="Neue Berechtigung erstellen">Berechtigung erstellen</a>
        </div>
    </div>
</div>