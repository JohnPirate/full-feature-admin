@role(['owner','administrator','developer'])
<nav class="gui-nav__responsive-container container--border-bottom gui-margin-xlarge-bottom gui-margin-xlarge-negative-top" role="navigation">
    <ul class="gui-nav gui-nav--center">
        <li role="presentation"><a href="{{ url('/admin/users') }}" title="Show users">Users</a></li>
        <li role="presentation"><a href="{{ url('/admin/roles') }}" title="Show roles">Roles</a></li>
        <li role="presentation"><a href="{{ url('/admin/permissions') }}" title="Show permissions">Permissions</a></li>
    </ul>
</nav>
@endrole