-- -----------------------------------------------------------------------------
-- Full Feature Admin
-- -----------------------------------------------------------------------------

-- -----------------------------------------------------------------------------
-- Modify laravel default users table if exists
-- -----------------------------------------------------------------------------
ALTER TABLE `users`
ADD COLUMN `email_confirmed` TINYINT(1) NOT NULL DEFAULT 0 AFTER `updated_at`,
ADD COLUMN `phone_number_confirmed` TINYINT(1) NOT NULL DEFAULT 0 AFTER `email_confirmed`,
ADD COLUMN `two_factor_enabled` TINYINT(1) NOT NULL DEFAULT 0 AFTER `phone_number_confirmed`,
ADD COLUMN `lockout_end` TIMESTAMP NULL DEFAULT NULL AFTER `two_factor_enabled`,
ADD COLUMN `lockout_enabled` TINYINT(1) NOT NULL DEFAULT 0 AFTER `lockout_end`,
ADD COLUMN `access_failed_count` INT(10) NOT NULL DEFAULT 0 AFTER `lockout_enabled`;


