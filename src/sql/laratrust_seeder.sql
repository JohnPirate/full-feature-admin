-- -----------------------------------------------------------------------------
-- Full Feature Admin
-- -----------------------------------------------------------------------------


-- -----------------------------------------------------------------------------
-- roles table seeder
-- -----------------------------------------------------------------------------
INSERT INTO `roles`
    (`name`, `display_name`, `description`, `created_at`, `updated_at`)
VALUES
    ('owner', 'Owner', 'Owner of the application', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP),
    ('administrator', 'Administrator', 'Administrator', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP),
    ('user', 'User', 'The default user', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);


-- -----------------------------------------------------------------------------
-- permissions table seeder
-- -----------------------------------------------------------------------------
INSERT INTO `permissions`
    (`name`, `display_name`, `description`, `created_at`, `updated_at`)
VALUES
    ('users-create', 'Create Users', 'Create Users', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP),
    ('users-read', 'Read Users', 'Read Users', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP),
    ('users-update', 'Update Users', 'Update Users', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP),
    ('users-delete', 'Delete Users', 'Delete Users', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP),
    ('roles-create', 'Create Roles', 'Create Roles', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP),
    ('roles-read', 'Read Roles', 'Read Roles', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP),
    ('roles-update', 'Update Roles', 'Update Roles', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP),
    ('role-permissions-update', 'Update Role-Permissions', 'Update Role-Permissions', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP),
    ('roles-delete', 'Delete Roles', 'Delete Roles', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP),
    ('permissions-create', 'Create Permissions', 'Create Permissions', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP),
    ('permissions-read', 'Read Permissions', 'Read Permissions', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP),
    ('permissions-update', 'Update Permissions', 'Update Permissions', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP),
    ('permission-roles-update', 'Update Permission-Roles', 'Update Permission-Roles', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP),
    ('permissions-delete', 'Delete Permission', 'Delete Permission', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP),
    ('users-lock', 'Lock User', 'Lock User', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP),
    ('users-unlock', 'Unlock User', 'Unlock User', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
