-- -----------------------------------------------------------------------------
-- Full Feature Admin
-- -----------------------------------------------------------------------------

-- -----------------------------------------------------------------------------
-- users table
-- -----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `users`
(
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `email` VARCHAR(255) NOT NULL,
  `password` VARCHAR(255) NOT NULL,
  `remember_token` VARCHAR(100) NULL,
  `created_at` TIMESTAMP NULL,
  `updated_at` TIMESTAMP NULL,
  `email_confirmed` TINYINT(1) NOT NULL DEFAULT 0,
  `phone_number_confirmed` TINYINT(1) NOT NULL DEFAULT 0,
  `two_factor_enabled` TINYINT(1) NOT NULL DEFAULT 0,
  `lockout_end` TIMESTAMP NULL,
  `lockout_enabled` TINYINT(1) NOT NULL DEFAULT 0,
  `access_failed_count` INT(10) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `users_name_unique` (`name` ASC),
  UNIQUE INDEX `users_email_unique` (`email` ASC)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_unicode_ci;

