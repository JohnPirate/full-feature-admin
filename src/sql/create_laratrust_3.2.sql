-- -----------------------------------------------------------------------------
-- LARATRUST - Role based system (v3.2.*)
-- -----------------------------------------------------------------------------

-- -----------------------------------------------------------------------------
-- Roles table
-- -----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `roles`
(
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `display_name` VARCHAR(255) NULL,
  `description` VARCHAR(255) NULL,
  `created_at` TIMESTAMP NULL,
  `updated_at` TIMESTAMP NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `roles_name_unique` (`name` ASC)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_unicode_ci;


-- -----------------------------------------------------------------------------
-- Permissions table
-- -----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `permissions`
(
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `display_name` VARCHAR(255) NULL,
  `description` VARCHAR(255) NULL,
  `created_at` TIMESTAMP NULL,
  `updated_at` TIMESTAMP NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `permissions_name_unique` (`name` ASC)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_unicode_ci;


-- -----------------------------------------------------------------------------
-- permission <--> role - pivot table
-- -----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `permission_role`
(
  `permission_id` INT(10) UNSIGNED NOT NULL,
  `role_id` INT(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`permission_id`, `role_id`),
  KEY `permission_role_role_id_foreign` (`role_id`),

  CONSTRAINT `permission_role_permission_id_foreign`
    FOREIGN KEY (`permission_id`)
    REFERENCES `permissions` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,

  CONSTRAINT `permission_role_role_id_foreign`
    FOREIGN KEY (`role_id`)
    REFERENCES `roles` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_unicode_ci;


-- -----------------------------------------------------------------------------
-- role <--> user - pivot table
-- -----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `role_user`
(
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  `user_type` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`user_id`, `role_id`, `user_type`),
  KEY `role_user_role_id_foreign` (`role_id`),

  CONSTRAINT `role_user_role_id_foreign`
    FOREIGN KEY (`role_id`)
    REFERENCES `roles` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_unicode_ci;


-- -----------------------------------------------------------------------------
-- permission <--> user - pivot table
-- -----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `permission_user`
(
  `permission_id` INT(10) UNSIGNED NOT NULL,
  `user_id` INT(10) UNSIGNED NOT NULL,
  `user_type` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`permission_id`, `user_id`, `user_type`),
  KEY `permission_user_permission_id_foreign` (`permission_id`),

  CONSTRAINT `permission_user_permission_id_foreign`
    FOREIGN KEY (`permission_id`)
    REFERENCES `permissions` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_unicode_ci;