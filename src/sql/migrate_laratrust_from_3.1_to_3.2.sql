-- -----------------------------------------------------------------------------
-- LARATRUST - Role based system (v3.2.*)
-- -----------------------------------------------------------------------------

-- -----------------------------------------------------------------------------
-- Migration from laratrust 3.1 to 3.2
-- -----------------------------------------------------------------------------
ALTER TABLE `role_user`
  DROP FOREIGN KEY `role_user_role_id_foreign`,
  DROP FOREIGN KEY `role_user_user_id_foreign`,
  DROP INDEX `role_user_role_id_foreign`,
  DROP PRIMARY KEY,
  ADD COLUMN `user_type` VARCHAR(255) NOT NULL;

UPDATE `role_user`
  SET `user_type` = 'App\\User';

ALTER TABLE `role_user`
  ADD PRIMARY KEY (`user_id`, `role_id`, `user_type`),
  ADD KEY `role_user_role_id_foreign` (`role_id`),
  ADD CONSTRAINT `role_user_role_id_foreign`
    FOREIGN KEY (`role_id`)
    REFERENCES `roles` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE;

ALTER TABLE `permission_user`
  DROP FOREIGN KEY `permission_user_user_id_foreign`,
  DROP FOREIGN KEY `permission_user_permission_id_foreign`,
  DROP INDEX `permission_user_user_id_foreign`,
  DROP PRIMARY KEY,
  ADD COLUMN `user_type` VARCHAR(255) NOT NULL;

UPDATE `permission_user`
  SET `user_type` = 'App\\User';

ALTER TABLE `permission_user`
  ADD PRIMARY KEY (`permission_id`, `user_id`, `user_type`),
  ADD KEY `permission_user_permission_id_foreign` (`permission_id`),
  ADD CONSTRAINT `permission_user_permission_id_foreign`
    FOREIGN KEY (`permission_id`)
    REFERENCES `permissions` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE;