<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;


class CheckIfUserIsLocked
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        /** @var \App\User $user */
        $user = $request->user();

        // Check if user is locked
        if ($user->isLocked()) {

            // Check if lock is out of time
            if ($user->isLockedOutOfTime()) {

                $user->unlock();
                $user->save();
            } else {

                return call_user_func(
                    Config::get('full_feature_admin.middleware_handling', 'abort'),
                    Config::get('full_feature_admin.middleware_params', '403')
                );
            }
        }
        return $next($request);

    }
}
