<?php

namespace FFA;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Laratrust\LaratrustRole;

/**
 * Class FFARole
 *
 * @package FFA
 * @author Stephan Sandriesser <stephan.sandriesser@gmail.com>
 */
class FFARole extends LaratrustRole
{
    const LARATRUST_ROLE_PERMISSION_CACHE = 'laratrust_permissions_for_role_';

    /**
     * Clears the laratrust role permissions cache for specific role
     */
    public function clearCache()
    {
        $key = self::LARATRUST_ROLE_PERMISSION_CACHE . "{$this->id}";

        Log::info('role-key', ['key'=>$key]);

        if (Cache::has($key)) {

            Cache::forget($key);
        }
    }
}