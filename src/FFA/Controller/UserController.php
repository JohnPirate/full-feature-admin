<?php

namespace App\Http\Controllers;

use Alert;
use Laratrust;
use Illuminate\Support\Facades\Log;
use App\Http\Requests\AdminUserUpdate;
use App\Role;
use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Http\Request;

/**
 * Class UserController
 * @package App\Http\Controllers
 */
class UserController extends Controller
{
    const PAGINATE_MAX = 15;

    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        $this->middleware(['auth', 'role:owner|administrator|developer']);
        $this->middleware('locked');
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        if (!Laratrust::can('users-read')) {
            return redirect('/missing-permission');
        }

        $templateValue = [];

        if ($query = $request->get('query')) {

            $users = User::where('name', 'like', "%{$query}%")
                ->orWhere('email', 'like', "%{$query}%")
                ->paginate(self::PAGINATE_MAX);
            $templateValue['query'] = $query;
        } else {

            $users = User::paginate(self::PAGINATE_MAX);
        }

        $templateValue['users'] = $users;


        return view('admin.users', $templateValue);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param                          $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function showUser(Request $request, $id)
    {
        if (!Laratrust::can('users-read')) {
            return redirect('/missing-permission');
        }

        $user = User::where('id', $id)->first();

        if (!$user) {
            Alert::warning("Es konnte kein User mit der ID '$id' gefunden werden.");
            return redirect('admin/users');
        }

        return view('admin.user-detail', [
            'user' => $user,
        ]);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param                          $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function editUser(Request $request, $id)
    {
        if (!Laratrust::can('users-update')) {
            return redirect('/missing-permission');
        }

        $user = User::where('id', $id)->first();

        if (!$user) {
            Alert::warning("Es konnte kein User mit der ID '$id' gefunden werden.");
            return redirect('admin/users');
        }

        $roles = Role::all();

        return view('admin.user-edit', [
            'user' => $user,
            'roles' => $roles,
        ]);
    }

    /**
     * @param \App\Http\Requests\AdminUserUpdate $request
     * @param                                    $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function updateUser(AdminUserUpdate $request, $id)
    {
        if (!Laratrust::can('users-update')) {
            return redirect('/missing-permission');
        }

        $user = User::where('id', $id)->first();

        if (!$user) {
            Alert::warning("Es konnte kein User mit der ID '$id' gefunden werden.");
            return redirect('admin/users');
        }

        $role_name = $request->get('userRole');
        $role = Role::where('name', $role_name)->first();

        if (!$role) {
            Alert::warning("Es konnte kein Benutzerrole mit der dem Namen '$role_name' gefunden werden.");
            return redirect('admin/users');
        }

        // Check if target user is 'owner' and auth user isn't 'owner'
        if ($user->hasRole('owner') && !Auth::user()->hasRole('owner')) {
            Alert::danger("Die Benutzerrolle des 'owner' kann nicht geändert werden!");
            Log::alert("User with ID " . Auth::user()->id . " is tried to change the owner role!");
            return redirect('admin/users');
        }

        // Check if role is 'owner' and auth user isn't 'owner
        if ($role->name === 'owner' && !Auth::user()->hasRole('owner')) {
            Alert::danger("Du kannst dich nicht selber zum 'owner' machen!!");
            Log::alert("User with ID " . Auth::user()->id . " is tried to change it selve to owner role!");
            return redirect('admin/users');
        }

        $user->roles()->detach();
        $user->roles()->attach($role->id);
        $user->clearCache();
        Alert::success("Update der Einstellungen erfolgreich");

        return redirect(sprintf('admin/users/%s/edit', $user->id));
    }

    /**
     * Locks the given user
     *
     * @param \Illuminate\Http\Request $request
     * @param                          $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function lockUser(Request $request, $id)
    {
        if (!Laratrust::can('users-lock')) {
            return redirect('/missing-permission');
        }

        $this->validate($request, [
            'userLockTime' => 'required|integer'
        ]);

        $user = User::where('id', $id)->first();

        if (!$user) {
            $msg = "Es konnte kein User mit der ID '$id' gefunden werden.";
            Alert::warning($msg);
            Log::warning($msg);
            return redirect('admin/users');
        }

        // Check if target user is 'owner'
        if ($user->hasRole('owner')) {
            Alert::danger("Der 'owner' kann nicht gesperrt werden!");
            Log::alert("User with ID " . Auth::user()->id . " is tried to lock the owner!");
            return redirect('admin/users');
        }

        $user->lockWithTime($request->input('userLockTime'));

        if ($user->save()) {
            $msg = "Der Benutzer mit der ID '" . $user->id  . "' wurde erfolgreich gesperrt bis: " . var_export($user->lockout_end, true);
            Alert::success($msg);
            Log::info($msg);
        } else {
            $msg = "Der Benutzer mit der ID '" . $user->id  . "' konnte nicht gesperrt werden.";
            Alert::danger($msg);
            Log::danger($msg);
        }

        return redirect(sprintf('admin/users/%s/edit', $user->id));
    }

    /**
     * Unlocks the given user
     *
     * @param \Illuminate\Http\Request $request
     * @param                          $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function unlockUser(Request $request, $id)
    {
        if (!Laratrust::can('users-unlock')) {
            return redirect('/missing-permission');
        }

        $user = User::where('id', $id)->first();

        if (!$user) {
            $msg = "Es konnte kein User mit der ID '$id' gefunden werden.";
            Alert::warning($msg);
            Log::warning($msg);
            return redirect('admin/users');
        }

        $user->unlock();

        if ($user->save()) {
            $msg = "Der Benutzer mit der ID '" . $user->id  . "' wurde erfolgreich entsperrt.";
            Alert::success($msg);
            Log::info($msg);
        } else {
            $msg = "Der Benutzer mit der ID '" . $user->id  . "' konnte nicht entsperrt werden.";
            Alert::danger($msg);
            Log::danger($msg);
        }

        return redirect(sprintf('admin/users/%s/edit', $user->id));
    }
}
