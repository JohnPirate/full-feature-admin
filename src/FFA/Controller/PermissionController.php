<?php

namespace App\Http\Controllers;

use Alert;
use Laratrust;
use Log;
use App\Http\Requests\PermissionStore;
use App\Http\Requests\PermissionUpdate;
use App\Permission;
use App\Role;
use Illuminate\Http\Request;

/**
 * Class PermissionController
 * @package App\Http\Controllers
 */
class PermissionController extends Controller
{
    const PAGINATE_MAX = 10;

    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        $this->middleware(['auth', 'role:owner|administrator|developer']);
        $this->middleware('locked');
    }

    /**
     * Show the permissions list page
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        if (!Laratrust::can('permissions-read')) {
            return redirect('/missing-permission');
        }

        $templateValue = [];

        if ($query = $request->get('query')) {

            $permissions = Permission::where('name', 'like', "%{$query}%")->paginate(self::PAGINATE_MAX);
            $templateValue['query'] = $query;
        } else {

            $permissions = Permission::paginate(self::PAGINATE_MAX);
        }

        $templateValue['permissions'] = $permissions;


        return view('admin.permissions', $templateValue);
    }

    /**
     * Show the permissions create page
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(Request $request)
    {
        if (!Laratrust::can('permissions-create')) {
            return redirect('/missing-permission');
        }

        return view('admin.permission-create');
    }

    /**
     * Store a new permission
     *
     * @param \App\Http\Requests\PermissionStore $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(PermissionStore $request)
    {
        if (!Laratrust::can('permissions-create')) {
            return redirect('/missing-permission');
        }

        $permission = new Permission();
        $permission->name = request('permissionName');
        $permission->display_name = request('permissionDisplayName');
        $permission->description = request('permissionDescription');

        if ($permission->save()) {
            Alert::success("Die Berechtigung '{$permission->display_name}' wurde hinzugefügt.");
        } else {
            Alert::danger('Die neue Berechtigung konnte nicht angelegt werden!');
        }

        return redirect(sprintf('/admin/permissions/%s/edit', $permission->id));
    }

    /**
     * Show the permission detail page
     *
     * @param \Illuminate\Http\Request $request
     * @param                          $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Request $request, $id)
    {
        if (!Laratrust::can('permissions-read')) {
            return redirect('/missing-permission');
        }

        return view('admin.permission-detail');
    }

    /**
     * Show the permission update page
     *
     * @param \Illuminate\Http\Request $request
     * @param                          $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Request $request, $id)
    {
        if (!Laratrust::can('permissions-update')) {
            return redirect('/missing-permission');
        }

        $permission = Permission::where('id', $id)->first();

        //dd($permission);

        if (!$permission) {
            Alert::warning("Es konnte keine Berechtigung mit der ID '$id' gefunden werden.");
            return redirect('/admin/permissions');
        }

        $roles = Role::all();

        // Get all permission related roles
        $permission_role_ids = [];
        foreach ($permission->roles as $role)
        {
            $permission_role_ids[] = $role->id;
        }

        return view('admin.permission-edit', [
            'permission' => $permission,
            'roles' => $roles,
            'permission_role_ids' => $permission_role_ids,
        ]);
    }

    /**
     * Update a permission
     *
     * @param \App\Http\Requests\PermissionUpdate $request
     * @param                                   $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(PermissionUpdate $request, $id)
    {
        if (!Laratrust::can('permissions-update')) {
            return redirect('/missing-permission');
        }

        $permission = Permission::where('id', $id)->first();

        if (!$permission) {
            Alert::warning("Es konnte keine Berechtigung mit der ID '$id' gefunden werden.");
            return redirect('/admin/permissions');
        }

        $permission->display_name = request('permissionDisplayName');
        $permission->description = request('permissionDescription');

        // Try to update permission
        if ($permission->save()) {
            Alert::success("Die Berechtigung '{$permission->display_name}' wurde geändert.");
        } else {
            Alert::danger('Berechtigung konnte nicht geändert werden.');
        }

        return redirect('/admin/permissions');
    }

    /**
     * Delete a permission
     *
     * @param \Illuminate\Http\Request $request
     * @param                          $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function delete(Request $request, $id)
    {
        if (!Laratrust::can('permissions-delete')) {
            return redirect('/missing-permission');
        }

        $permission = Permission::where('id', $id)->first();

        if (!$permission) {
            Alert::warning("Es konnte keine Berechtigung mit der ID '$id' gefunden werden.");
            return redirect('/admin/permissions');
        }

        // Remove all permission-role relations
        $permission->roles()->detach();

        // Get role display name before delete
        $permissionName = $permission->display_name;

        // Try to delete permission
        if ($permission->delete()) {
            Alert::success("Die Berechtigung '{$permissionName}' wurde gelöscht.");
        } else {
            Alert::danger('Berechtigung konnte nicht gelöscht werden.');
        }

        return redirect('/admin/permissions');
    }

    /**
     * Update the permission related roles
     *
     * @param \Illuminate\Http\Request $request
     * @param                          $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function updateRoles(Request $request, $id)
    {
        if (!Laratrust::can('permission-roles-update')) {
            return redirect('/missing-permission');
        }

        $permission = Permission::where('id', $id)->first();

        if (!$permission) {
            Alert::warning("Es konnte keine Berechtigung mit der ID '$id' gefunden werden.");
            return redirect('/admin/permissions');
        }

        $role_ids = request('roles') ? array_keys(request('roles')) : [];

        if (is_array($role_ids)) {
            $permission->roles()->sync($role_ids);

            foreach (Role::all() as $role) {
                $role->clearCache();
            }

            Alert::success("Update der Benutzerrollen erfolgreich");
        } else {
            Alert::danger("Fehler bei den Benutzerrollen aufgetreten");
        }

        return redirect(sprintf('/admin/permissions/%s/edit', $id));
    }
}
