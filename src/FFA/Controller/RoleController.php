<?php

namespace App\Http\Controllers;

use Alert;
use Laratrust;
use Log;
use App\Http\Requests\RoleStore;
use App\Http\Requests\RoleUpdate;
use App\Permission;
use App\Role;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        $this->middleware(['auth', 'role:owner|administrator|developer']);
        $this->middleware('locked');
    }

    /**
     * Show the role list page
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        if (!Laratrust::can('roles-read')) {
            return redirect('/missing-permission');
        }

        $roles = Role::all();

        return view('admin.roles', [
            'roles' => $roles,
        ]);
    }

    /**
     * Show the role create page
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(Request $request)
    {
        if (!Laratrust::can('roles-create')) {
            return redirect('/missing-permission');
        }

        return view('admin.role-create');
    }

    /**
     * Store a new role
     *
     * @param \App\Http\Requests\RoleStore $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(RoleStore $request)
    {
        if (!Laratrust::can('roles-create')) {
            return redirect('/missing-permission');
        }

        $role = new Role();
        $role->name = $request->input('roleName');
        $role->display_name = $request->input('roleDisplayName');
        $role->description = $request->input('roleDescription');

        if ($role->save()) {
            Alert::success("Die Benutzerrolle '{$role->display_name}' wurde hinzugefügt.");
        } else {
            Alert::danger('Die neue Benutzerrolle konnte nicht angelegt werden!');
        }

        return redirect(sprintf('/admin/roles/%s/edit', $role->id));
    }

    /**
     * Show the role detail page
     *
     * @param \Illuminate\Http\Request $request
     * @param                          $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Request $request, $id)
    {
        if (!Laratrust::can('roles-read')) {
            return redirect('/missing-permission');
        }

        return view('admin.role-detail');
    }

    /**
     * Show the role update page
     *
     * @param \Illuminate\Http\Request $request
     * @param                          $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Request $request, $id)
    {
        if (!Laratrust::can('roles-update')) {
            return redirect('/missing-permission');
        }

        $role = Role::where('id', $id)->first();

        if (!$role) {
            Alert::warning("Es konnte keine Benutzerrolle mit der ID '$id' gefunden werden.");
            return redirect('/admin/roles');
        }

        $permissions = Permission::all();

        // Get all role related permission id's
        $role_permission_ids = [];
        foreach ($role->permissions as $role_permission)
        {
            $role_permission_ids[] = $role_permission->id;
        }

        return view('admin.role-edit', [
            'role' => $role,
            'permissions' => $permissions,
            'role_permission_ids' => $role_permission_ids,
        ]);
    }

    /**
     * Update a role
     *
     * @param \App\Http\Requests\RoleUpdate $request
     * @param                               $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(RoleUpdate $request, $id)
    {
        if (!Laratrust::can('roles-update')) {
            return redirect('/missing-permission');
        }

        $role = Role::where('id', $id)->first();

        if (!$role) {
            Alert::warning("Es konnte keine Benutzerrolle mit der ID '$id' gefunden werden.");
            return redirect('/admin/roles');
        }

        $role->display_name = $request->input('roleDisplayName');
        $role->description = $request->input('roleDescription');

        // Try to update permission
        if ($role->save()) {
            Alert::success("Die Benutzerrolle '{$role->display_name}' wurde geändert.");
        } else {
            Alert::danger('Benutzerrolle konnte nicht geändert werden.');
        }

        return redirect('/admin/roles');
    }

    /**
     * Delete a role
     *
     * @param \Illuminate\Http\Request $request
     * @param                          $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function delete(Request $request, $id)
    {
        if (!Laratrust::can('roles-delete')) {
            return redirect('/missing-permission');
        }

        $role = Role::where('id', $id)->first();

        if (!$role) {
            Alert::warning("Es konnte keine Benutzerrolle mit der ID '$id' gefunden werden.");
            return redirect('/admin/roles');
        }

        // Remove all permission-role relations
        $role->permissions()->detach();

        // Remove all role-user relations
        $role->users()->detach();

        // Get role display name before delete
        $roleName = $role->display_name;

        // Try to delete permission
        if ($role->delete()) {
            Alert::success("Die Benutzerrolle '{$roleName}' wurde gelöscht.");
        } else {
            Alert::danger('Benutzerrolle konnte nicht gelöscht werden.');
        }

        return redirect('/admin/roles');
    }

    /**
     * Update the role related permissions
     *
     * @param \Illuminate\Http\Request $request
     * @param                          $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function updatePermissions(Request $request, $id)
    {
        if (!Laratrust::can('role-permissions-update')) {
            return redirect('/missing-permission');
        }

        $role = Role::where('id', $id)->first();

        if (!$role) {
            Alert::warning("Es konnte keine Benutzerrolle mit der ID '$id' gefunden werden.");
            return redirect('/admin/roles');
        }

        $permission_ids = $request->get('permissions') ? array_keys($request->get('permissions')) : [];

        if (is_array($permission_ids)) {
            $role->permissions()->sync($permission_ids);
            $role->clearCache();

            Alert::success("Update der Berechtigungen erfolgreich");
        } else {
            Alert::danger("Fehler bei den Berechtigungdaten aufgetreten");
        }


        return redirect(sprintf('/admin/roles/%s/edit', $id));
    }
}
