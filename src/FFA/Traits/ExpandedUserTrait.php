<?php

namespace FFA\Traits;

use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;

/**
 * Class ExpandedUserTrait
 *
 * @package FFA\Traits
 * @author Stephan Sandriesser <stephan.sandriesser@gmail.com>
 */
trait ExpandedUserTrait
{
    /**
     * Clears the laratrust user roles cache for specific user
     */
    public function clearCache()
    {
        $key = 'laratrust_roles_for_user_' . $this->id;

        if (Cache::has($key)) {

            Cache::forget($key);
        }
    }

    /**
     * Checks if the current user is locked;
     *
     * @return bool
     */
    public function isLocked()
    {
        return $this->lockout_enabled === 1;
    }

    /**
     * Checks if the user lock is out of time
     *
     * @return bool
     */
    public function isLockedOutOfTime()
    {
        // Check if user is permanent locked
        if ($this->isLocked() && $this->lockout_end === null) {
            return false;
        }
        $timeNow = Carbon::now('Europe/Vienna');
        $lockoutEnd = Carbon::createFromFormat('Y-m-d H:i:s', $this->lockout_end, 'Europe/Vienna');
        return $timeNow->gt($lockoutEnd);
    }

    /**
     * Locks the current user permanent
     */
    public function lock()
    {
        $this->lockWithTimestamp(null);
    }

    /**
     * Locks the current user on time
     *
     * @param $timestamp
     */
    public function lockWithTimestamp($timestamp = null)
    {
        $this->lockout_enabled = true;
        $this->lockout_end = $timestamp;
    }

    /**
     * Locks the current user to time of minutes
     *
     * @param $minutes
     */
    public function lockWithTime($minutes)
    {
        $timestamp = null;
        if ($minutes > 0) {
            $timestamp = Carbon::now('Europe/Vienna')
                ->addMinutes($minutes)
                ->toDateTimeString();
        }
        $this->lockWithTimestamp($timestamp);
    }

    /**
     * Unlocks the current user
     */
    public function unlock()
    {
        $this->lockout_enabled = false;
        $this->lockout_end = null;
    }
}