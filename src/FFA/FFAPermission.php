<?php

namespace FFA;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Laratrust\LaratrustPermission;

/**
 * Class FFAPermission
 *
 * @package FFA
 * @author Stephan Sandriesser <stephan.sandriesser@gmail.com>
 */
class FFAPermission extends LaratrustPermission
{

}